package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	//Servlet LifeCycles
	//Initialization is the first stage of the servlet lifecycle.
	//This stage is when the servlet is first initialized upon use:
	//-loads the servlet class
	//-creates an instance of the servlet class
	//-the init() method initializes the servlet instance
	
	public void init() throws ServletException {
		System.out.println("********************");
		System.out.println("Initialized Servlet.");
		System.out.println("Connected to DB.");
		System.out.println("********************");
	}
	//doPost previously service, pag doPost working lang kapag nakaPOST method yung form sa index.html
	//service methods(doPost, service()) second stage of the servlet lifecycle that handles requests and responses
	//doPost() service methods handles post methods requests
	//service() handles any requeest
	//doPut~ doDelete other methods
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		System.out.println("Hello from the Calculator Servlet");
		
		//To get the data passed from the input to our query string we have to get the getParameter method of our request.
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		
		//system out shortcut: type sysout and then press ctrl + space
		System.out.println("The sum of the two numbers are:");
		System.out.println(num1+num2);
		
		
		//getWriter method from the response to produce an output in the page
		
		PrintWriter out = res.getWriter();
		
		//Create a total variable;
		int total = num1 + num2;
		
		//out.println("The total of two numbers are: "+ total);		
		//getWriter can output not only string but also HTML elements
		out.println("<h1>The total number of two numbers are: " + total + "</h1>");
	}
	
	//doGet() service methods handles get  method requests
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out = res.getWriter();
		out.println("<h1>You have access the GET method service method of the calculator servlet </h1>");
	}
	
	//finalization - the last stage of a servlet's lifecycle
	// - Cleanup resources once the servlet is destroyed or unused
	// - Close connections
	// the destroy() method finalizes the servlet
	
	public void destroy() {
		System.out.println("****************");
		System.out.println("Finalizing Servlet");
		System.out.println("Disconnected from DB");
		System.out.println("****************");
	}
	
}









