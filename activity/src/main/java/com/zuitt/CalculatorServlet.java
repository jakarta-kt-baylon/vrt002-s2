package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException {
		System.out.println("********************");
		System.out.println("Initialized Servlet.");
		System.out.println("Connected to DB.");
		System.out.println("********************");
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		System.out.println("Hello from the Calculator Servlet");
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		System.out.println(operation);
		
		int total = 0;
		
		PrintWriter out = res.getWriter();
		
		switch (operation) {
		  case "add":
			  total = num1 + num2;
			  out.println(
					  "<h1>The two numbers you provided are: " + num1 +", "+ num2 + "</h1>" +
					  "<h1>The operation that you wanted is: " + operation + "</h1>"+
					  "<h1>The result is: " + total + "</h1>"
					  );
		    break;
		  case "subtract":
			  total = num1 - num2;
			  out.println(
					  "<h1>The two numbers you provided are: " + num1 +", "+ num2 + "</h1>" +
					  "<h1>The operation that you wanted is: " + operation + "</h1>"+
					  "<h1>The result is: " + total + "</h1>"
					  );
		    break;
		  case "multiply":
			  total = num1 * num2;
			  out.println(
					  "<h1>The two numbers you provided are: " + num1 +", "+ num2 + "</h1>" +
					  "<h1>The operation that you wanted is: " + operation + "</h1>"+
					  "<h1>The result is: " + total + "</h1>"
					  );
		    break;
		  case "divide":
			  total = num1 / num2;
			  out.println(
					  "<h1>The two numbers you provided are: " + num1 +", "+ num2 + "</h1>" +
					  "<h1>The operation that you wanted is: " + operation + "</h1>"+
					  "<h1>The result is: " + total + "</h1>"
					  );
		    break;
		}
		
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app </h1> <p>To use the app, input two numbers and an operation. <br> Hit the submit button after filling in the details. <br> You will get the result shown in your browser! </p>");
	}
	
	
	public void destroy() {
		System.out.println("****************");
		System.out.println("Finalizing Servlet");
		System.out.println("****************");
	}

}
